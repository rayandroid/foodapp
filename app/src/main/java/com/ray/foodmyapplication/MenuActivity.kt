package com.ray.foodmyapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ray.foodmyapplication.adapater.ItemAdapater
import com.ray.foodmyapplication.data.ItemMenu


class MenuActivity : AppCompatActivity() {
    private  lateinit var recyclerView: RecyclerView
    private lateinit var  itemAdapater: ItemAdapater
    private  var menuList = mutableListOf<ItemMenu>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        recyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = GridLayoutManager(applicationContext,2)
        itemAdapater = ItemAdapater(applicationContext)
        recyclerView.adapter = itemAdapater


        menuList.add(ItemMenu("น้ำมะเขือเทศ", R.drawable.a))
        menuList.add(ItemMenu("น้ำแอปเปิ้ล", R.drawable.aa))
        menuList.add(ItemMenu("น้ำแครอท", R.drawable.b))
        menuList.add(ItemMenu("น้าขิงน้ำผิงมะนาว", R.drawable.dd))
        menuList.add(ItemMenu("น้ำลิ้นจี่เพื่อสุขภาพ", R.drawable.e))
        menuList.add(ItemMenu("น้ำเลมอน", R.drawable.p))
        menuList.add(ItemMenu("น้ำผลไม้รวม", R.drawable.rr))
        menuList.add(ItemMenu("น้ำมะนาว", R.drawable.t,))
        menuList.add(ItemMenu("น้ำขิงกระวาน", R.drawable.y))
        menuList.add(ItemMenu("น้ำส้มคั้น", R.drawable.k))
        menuList.add(ItemMenu("น้ำมะนาวกระวาน", R.drawable.m))
        menuList.add(ItemMenu("น้ำมะนาวมิ้น", R.drawable.w))

        itemAdapater.setDataList(menuList)

    }



}

