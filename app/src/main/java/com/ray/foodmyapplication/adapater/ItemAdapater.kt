package com.ray.foodmyapplication.adapater

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ray.foodmyapplication.*
import com.ray.foodmyapplication.data.ItemMenu
import com.ray.foodmyapplication.model.DetailedActivity
import com.ray.foodmyapplication.model.DetailedActivity2


class ItemAdapater(val context: Context): RecyclerView.Adapter<ItemAdapater.ViewHolder> () {
    var menuList = emptyList<ItemMenu>()
    internal fun setDataList(menuList: List<ItemMenu>) {
        this.menuList = menuList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var title: TextView

        init {
            image = itemView.findViewById(R.id.image)
            title = itemView.findViewById(R.id.title)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_grid, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = menuList.size


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = menuList[position]

        holder.title.text = data.title
        holder.image.setImageResource(data.image)
        holder.image.setOnClickListener {
            if (position == 0) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity::class.java)
                context.startActivity(intent)

            }
            if (position == 1) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity2::class.java)
                context.startActivity(intent)


            }
            if (position == 2) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity3::class.java)
                context.startActivity(intent)
            }
            if (position == 3) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity4::class.java)
                context.startActivity(intent)
            }
            if (position == 4) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity5::class.java)
                context.startActivity(intent)
            }
            if (position == 5) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity6::class.java)
                context.startActivity(intent)
            }
            if (position == 6) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity7::class.java)
                context.startActivity(intent)
            }
            if (position == 7) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity8::class.java)
                context.startActivity(intent)
            }
            if (position == 8) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity9::class.java)
                context.startActivity(intent)
            }
            if (position == 9) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity10::class.java)
                context.startActivity(intent)
            }
            if (position == 10) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity11::class.java)
                context.startActivity(intent)
            }
            if (position == 11) {
                val context = holder.image.context
                val intent = Intent(context, DetailedActivity12::class.java)
                context.startActivity(intent)
            }
        }
    }
}
