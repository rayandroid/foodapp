package com.ray.foodmyapplication.model

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.ray.foodmyapplication.*

class DetailedActivity : AppCompatActivity() {

    var count = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailed)
        var btnok : Button? = null

        btnok = findViewById<Button>(R.id.BtnOk)
        btnok!!.setOnClickListener{
            var intent = Intent(this,ShopingActivity::class.java)
            startActivity(intent)
        }
    }

    fun Tap(view: android.view.View) {
        count++
        val display_text: TextView = findViewById(R.id.value)
        display_text.setText(count.toString())

    }

    fun Tapp(view: android.view.View) {
        count--
        val display_text: TextView = findViewById(R.id.value)
        display_text.setText(count.toString())

    }



}